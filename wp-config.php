<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tg' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'wpqWI(W(?By,snBu7h~`yt_y&iqh>xl@:?;jN1eP0Ie:Uz-hj rVyEMp08o<EUV%' );
define( 'SECURE_AUTH_KEY',  'M?3YMHj<oYcY<}A-?B3 k%H&<^kEoI(c-.~G2RGe$jNY(!xGG#?0OR {Ca*@_E12' );
define( 'LOGGED_IN_KEY',    'B>.Wbi2U$RxnfS]ql%jahni3 =![y!udU;bnLzOMe[Fqs[X+;HPsFX}!F$7`7(?q' );
define( 'NONCE_KEY',        'Cj+ph#JGaD8j8U<#1,Y]t2aAa/N;}Sc^#!(tvcmT6%9c e[gKOMQV!:4>p!I_7tL' );
define( 'AUTH_SALT',        '6*ov^)LJ6nHunt*gTW:p/n$X[C<hI%A$ve1K?f5i*X] 0uOti%!hzFhzJI]:{Fy<' );
define( 'SECURE_AUTH_SALT', 'o[|(s4p$ZBvp2[mtV!mI]Qjb6dy!HIs&4>WKt3>X j6-+N>ED.6~9st=M+XlI8M#' );
define( 'LOGGED_IN_SALT',   'r5 Dq.U>[xtFBw{ga#/Ev6vMA$p4+@*ipemwA_QVUxf}fx%Nk~>VF*qnG{dvd.xZ' );
define( 'NONCE_SALT',       'x.twyUlU=- JG2wfY#vP-whzPXPkR4otJ3eK35,SOD9_f5!.r5))*:YWct6}]y2r' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
