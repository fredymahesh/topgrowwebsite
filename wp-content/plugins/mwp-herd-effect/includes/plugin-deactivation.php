<?php
/**
 * Deactivation function
 *
 * @package     Wow_Plugin
 * @subpackage  Includes/Dectivation
 * @author      Dmytro Lobov <i@wpbiker.com>
 * @copyright   2019 Wow-Company
 * @license     GNU Public License
 * @version     1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$info   = self::information();
$prefix = $info['plugin']['prefix'];
$name   = $info['plugin']['name'];
$url    = $info['url']['author'];


$license    = trim( get_option( 'wow_license_key_' . $prefix ) );
$api_params = array(
	'edd_action' => 'deactivate_license',
	'license'    => $license,
	'item_name'  => urlencode( $name ),
	'url'        => home_url(),
);

$response   = wp_remote_post( $url, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
// decode the license data
$license_data = json_decode( wp_remote_retrieve_body( $response ) );
// $license_data->license will be either "deactivated" or "failed"
if ( $license_data->license == 'deactivated' ) {
	delete_option( 'wow_license_status_' . $prefix );
}
