<?php
/**
 * Update plugin data to new version
 *
 * @package     Wow_Plugin
 * @copyright   Copyright (c) 2018, Dmytro Lobov
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

namespace herd_effects;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$info = self::information();

if ( get_option( 'wow_' . $info['plugin']['prefix'] . '_updater_3' ) === false ) {

	global $wpdb;
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	$table = $wpdb->prefix . 'wow_' . $info['plugin']['prefix'];

	$sql = "CREATE TABLE " . $table . " (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		title VARCHAR(200) NOT NULL,
		param TEXT,
		script TEXT,
		style TEXT,
		UNIQUE KEY id (id)
	) DEFAULT CHARSET=utf8;";
	dbDelta( $sql );

	$result = $wpdb->get_results( "SELECT * FROM " . $table . " order by id asc" );
	if ( count( $result ) > 0 ) {
		$script = '(function ($) {';
		$style  = null;

		foreach ( $result as $key => $val ) {
			$param = unserialize( $val->param );

			$file_script = $info['plugin']['dir'] . 'admin/partials/generator/script.php';
			if ( file_exists( $file_script ) ) {
				ob_start();
				include( $file_script );
				$content_script = ob_get_contents();
				ob_end_clean();
				$wpdb->update( $table, array( 'script' => $content_script ), array( 'id' => $val->id ) );
				$script .= $content_script;
			}

			$file_style = $info['plugin']['dir'] . 'admin/partials/generator/style.php';
			if ( file_exists( $file_style ) ) {
				ob_start();
				include( $file_style );
				$content_style = ob_get_contents();
				ob_end_clean();
				$wpdb->update( $table, array( 'style' => $content_style ), array( 'id' => $val->id ) );
				$style .= $content_style;
			}

		}
		$basedir = $info['plugin']['dir'] . '/assets/public/';
		$script .= '})(jQuery)';
		$script_packer  = __NAMESPACE__ . '\\JavaScriptPacker';
		$packer         = new $script_packer( $script, 'Normal', true, false );
		$packed         = $packer->pack();

		if ( ! empty( $packed ) ) {
			$path_script = $basedir . 'script.js';
			file_put_contents( $path_script, $packed );
		}
		if ( ! empty( $style ) ) {
			$path_style = $basedir . 'style.css';
			file_put_contents( $path_style, $style );
		}
	}

	update_option( 'wow_' . $info['plugin']['prefix'] . '_updater_3', '3.0' );
}
