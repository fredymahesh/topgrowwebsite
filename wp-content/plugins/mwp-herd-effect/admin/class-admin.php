<?php

/**
 * Admin Class
 *
 * @package     Wow_Plugin
 * @subpackage  Admin
 * @author      Dmytro Lobov <i@wpbiker.com>
 * @copyright   2019 Wow-Company
 * @license     GNU Public License
 * @version     1.0
 */

namespace herd_effects;


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wow_Plugin_Admin
 *
 * @package wow_plugin
 *
 * @property array plugin - base information about the plugin
 * @property array url    - home, pro and other URL for plugin
 * @property array rating - website and link for rating
 *
 */
class Wow_Plugin_Admin {

	/**
	 * Setup to admin panel of the plugin
	 *
	 * @param array $info general information about the plugin
	 *
	 * @since 1.0
	 */
	public function __construct( $info ) {
		$this->plugin = $info['plugin'];
		$this->url    = $info['url'];
		$this->rating = $info['rating'];

		add_filter( 'plugin_action_links', array( $this, 'settings_link' ), 10, 2 );
		add_filter( 'admin_footer_text', array( $this, 'footer_text' ) );
		add_action( 'admin_menu', array( $this, 'add_admin_page' ) );
		add_action('admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		add_action( 'wp_ajax_herd_effect_message', array( $this, 'ad_message_deactivate' ) );
		add_action( 'wp_ajax_' . $this->plugin['prefix'] . '_item_save', array( $this, 'item_save' ) );
	}


	/**
	 * Add the link to the plugin page on Plugins page
	 *
	 * @param $actions
	 * @param $plugin_file - the plugin main file
	 *
	 * @return mixed
	 */
	public function settings_link( $actions, $plugin_file ) {
		if ( false === strpos( $plugin_file, plugin_basename( $this->plugin['file'] ) ) ) {
			return $actions;
		}
		$link          = admin_url( 'admin.php?page=' . $this->plugin['slug'] );
		$text          = esc_attr__( 'Settings', $this->plugin['text'] );
		$settings_link = '<a href="' . esc_url( $link ) . '">' . esc_attr( $text ) . '</a>';
		array_unshift( $actions, $settings_link );

		return $actions;
	}

	/**
	 * Add custom text in the footer on the wow plugin page
	 *
	 * @param $footer_text - text in the footer
	 *
	 * @return string - end text in the footer
	 * @since 1.0
	 */
	public function footer_text( $footer_text ) {
		global $wow_plugin_page;
		if ( $wow_plugin_page == $this->plugin['slug'] ) {
			$text = sprintf(
				__( 'Thank you for using <a href="%1$s" target="_blank">%2$s</a>! Please <a href="%3$s" target="_blank">rate us on %4$s</a>', $this->plugin['text'] ),
				esc_url( $this->url['home'] ),
				esc_attr( $this->plugin['name'] ),
				esc_url( $this->rating['url'] ),
				esc_attr( $this->rating['website'] )
			);
			return str_replace( '</span>', '', $footer_text ) . ' | ' . $text . '</span>';
		} else {
			return $footer_text;
		}
	}

	/**
	 * Add the plugin page in admin menu
	 *
	 * @since 1.0
	 */
	public function add_admin_page() {
		$parent     = 'wow-company';
		$title      = $this->plugin['name'] . ' version ' . $this->plugin['version'];
		$menu_title = $this->plugin['menu'];
		$capability = 'manage_options';
		$slug       = $this->plugin['slug'];
		add_submenu_page( $parent, $title, $menu_title, $capability, $slug, array( $this, 'plugin_page' ) );
	}

	/**
	 * Include main plugin page
	 *
	 * @since 1.0
	 */
	public function plugin_page() {
		global $wow_plugin_page;
		$wow_plugin_page = $this->plugin['slug'];
		require_once 'page-main.php';
	}

	/**
	 * Include admin style and scripts on the plugin page
	 *
	 * @since 1.0
	 */
	public function admin_scripts($hook) {
		$page = 'wow-plugins_page_' . $this->plugin['slug'];

		if ( $page != $hook ) {
			return;
		}

		$slug       = $this->plugin['slug'];
		$version    = $this->plugin['version'];
		$url_assets = plugin_dir_url( __FILE__ ) . 'assets/';
		$pre_suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		wp_enqueue_style( $slug . '-admin', $url_assets . 'css/main.min.css', false, $version );
		wp_enqueue_style( $slug . '-custom', $url_assets . 'css/custom.css', false, $version );
		wp_enqueue_style( $slug . '-preview', $url_assets . 'css/preview.min.css', false, $version );

		// include the color picker
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );

		// include an alpha rgba color picker
		$url_alpha = $url_assets . 'js/wp-color-picker-alpha.min.js';
		 wp_enqueue_script( 'wp-color-picker-alpha', $url_alpha, array( 'wp-color-picker' ) );

		$url_vendors = $this->plugin['url'] . 'vendors/';
		// include fontAwesome icon
		$url_fontawesome = $url_vendors . 'fontawesome/css/fontawesome-all.min.css';
		wp_enqueue_style( $slug . '-fontawesome', $url_fontawesome, null, '5.12' );

		// include fonticonpicker styles & scripts
		$fonticonpicker_js = $url_assets . 'fonticonpicker/fonticonpicker.min.js';
		wp_enqueue_script( $slug . '-fonticonpicker', $fonticonpicker_js, array( 'jquery' ) );

		$fonticonpicker_css = $url_assets . 'fonticonpicker/css/fonticonpicker.min.css';
		wp_enqueue_style( $slug . '-fonticonpicker', $fonticonpicker_css );

		$fonticonpicker_dark_css = $url_assets . 'fonticonpicker/fonticonpicker.darkgrey.min.css';
		wp_enqueue_style( $slug . '-fonticonpicker-darkgrey', $fonticonpicker_dark_css );

		// include the plugin admin script
		$url_script = plugin_dir_url( __FILE__ ) . 'assets/js/admin-script.min.js';
		wp_enqueue_script( $slug . '-admin', $url_script, array( 'jquery' ), $version, true );

		$url_preview = plugin_dir_url( __FILE__ ) . 'assets/js/preview.min.js';
		wp_enqueue_script( $slug . '-preview', $url_preview, array( 'jquery' ), $version, true );


	}

	/**
	 * Deactivate the Ad message on plugin page
	 *
	 * @since 1.0
	 */
	public function ad_message_deactivate() {
		update_option( 'wow_' . $this->plugin['prefix'] . '_message', 'read' );
		wp_die();
	}

	static function input( $arg ) {
		include ('fields/input.php');
	}

	static function number( $arg ) {
		include ('fields/number.php');
	}

	static function select( $arg ) {
		include ('fields/select.php');
	}

	static function editor( $arg ) {
		include ('fields/editor.php');
	}

	static function textarea( $arg ) {
		include ('fields/textarea.php');
	}

	static function color( $arg ) {
		include ('fields/color.php');
	}

	static function checkbox( $arg ) {
		include ('fields/checkbox.php');
	}

	static function time( $arg ) {
		include ('fields/time.php');
	}

	static function date( $arg ) {
		include ('fields/date.php');
	}

	public function pro( $arg ) {
		include ('fields/pro.php');
	}

	public function convert_old_time($old_time) {
		$new_time = '';
		if ($old_time < 10) {
			$new_time = '0'.$old_time.':00';
		} else {
			$new_time = $old_time.':00';
		}
		return $new_time;
	}


	/**
	 * @param string $tooltip tooltip for element
	 *
	 * @return string
	 */

	/*public function pro( $tooltip = null ) {
		$link    = admin_url() . 'admin.php?page=' . $this->plugin['slug'] . '&tab=extension';
		$title   = esc_attr__( 'More features in the PRO version', $this->plugin['text'] );
		$classes = 'wow-help dashicons dashicons-lock';
		$tooltip = ! empty( $tooltip ) ? $title . '<br/>' . $tooltip : $title;
		$pro     = '<a href="' . $link . '" class="' . $classes . '" title="' . $tooltip . '"></a>';
		return $pro;
	}*/


	/**
	 * Save and Update the Item into the plugin Database
	 *
	 * @return array response from DB
	 *
	 * @since 1.0
	 */
	public function save_data() {
		$save_class    = __NAMESPACE__ . '\\Wow_DB_Update';
		$objItem       = new $save_class( $this->plugin['dir'], $this->plugin['slug'] );
		$add           = ( isset( $_REQUEST['add'] ) ) ? absint( $_REQUEST['add'] ) : '';
		$table         = ( isset( $_REQUEST['data'] ) ) ? sanitize_text_field( $_REQUEST['data'] ) : '';
		$tool_id       = absint( $_POST['tool_id'] );
		$info          = array();
		$info['id']    = $tool_id;
		$info['title'] = wp_unslash( sanitize_text_field( $_POST['title'] ) );
		$info_arr      = $_POST['param'];
		$info['param'] = array();
		foreach ( $info_arr as $key => $val ) {
			$info['param'][ $key ] = wp_unslash( $val );
		}
		$response = '';
		if ( '1' == $add ) {
			$objItem->create_item( $table, $info );
			$response = array(
				'status'  => 'OK',
				'message' => esc_attr__( 'Item Added', $this->plugin['text'] ),
			);
		} elseif ( '2' == $add ) {

			$objItem->update_item( $table, $info );
			$response = array(
				'status'  => 'OK',
				'message' => esc_attr__( 'Item Updated', $this->plugin['text'] ),
			);
		}
		return $response;
	}

	public function item_save() {
		$response = 'No';
		if ( isset( $_POST[ $this->plugin['slug'] . '_nonce' ] ) ) {
			if ( ! empty( $_POST )
			     && wp_verify_nonce( $_POST[ $this->plugin['slug'] . '_nonce' ], $this->plugin['slug'] . '_action' )
			     && current_user_can( 'manage_options' )
			) {
				$response = self:: save_data();
			}
		}
		wp_send_json( $response );
		wp_die();
	}

}
