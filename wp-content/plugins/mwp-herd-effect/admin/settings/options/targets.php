<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Tergeting settings
 *
 * @package     Lead_Generation
 * @subpackage  Settings
 * @copyright   Copyright (c) 2018, Dmytro Lobov
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */


//region Schedule
$weekday = array(
	'label'   => esc_attr__( 'Day of the week', $this->plugin['text'] ),
	'attr'    => [
		'type'  => 'select',
		'value' => esc_attr__( 'Everyday', $this->plugin['text'] ),
	],
	'tooltip'    => esc_attr__( 'Select the day of the week when the notification will be displayed.', $this->plugin['text'] ),
	'icon'    => '',
	'func'    => '',
);


$time_start = array(
	'label' => esc_attr__( 'Time from', $this->plugin['text'] ),
	'attr'  => [
		'type'        => 'time',
		'value'       => '00:00',
	],
	'tooltip'  => esc_attr__( 'Specify what from time of the day to show the notice', $this->plugin['text'] ),
	'icon'  => '',
);

$time_end = array(
	'label' => esc_attr__( 'Time to', $this->plugin['text'] ),
	'attr'  => [
		'type'        => 'time',
		'value'       => '23:59',
	],
	'tooltip'  => esc_attr__( 'Specify what to time of the day to show the notice.', $this->plugin['text'] ),
	'icon'  => '',
);

$set_dates = array(
	'label' => esc_attr__( 'Set Dates', $this->plugin['text'] ),
	'attr'  => [
		'type'        => 'checkbox',
		'value'       =>'',
	],
	'tooltip'  => esc_attr__( 'Check this if you want to set the show notification between dates.', $this->plugin['text'] ),
	'icon'  => '',
	'func'  => 'setDate',
);

//endregion

//region Devices
$screen_more = array(
	'label'    => esc_attr__( 'Don\'t show on screens more', $this->plugin['text'] ),
	'attr'     => [
		'name'  => 'param[screen_more]',
		'id'    => 'screen_more',
		'value' => isset( $param['screen_more'] ) ? $param['screen_more'] : '1024',
		'min'   => '0',
		'step'  => '1',
	],
	'checkbox' => [
		'name'  => 'param[include_more_screen]',
		'id'    => 'include_more_screen',
		'value' => isset( $param['include_more_screen'] ) ? $param['include_more_screen'] : 0,
	],
	'addon'    => [
		'unit' => 'px',
	],
	'tooltip'     => esc_attr__( 'Specify the window breakpoint when the notification will be shown', $this->plugin['text'] ),
);

$mobil_show = ! empty( $param['mobil_show'] ) ? $param['mobil_show'] : 0;

$screen = array(
	'label'    => esc_attr__( 'Don\'t show on screens less', $this->plugin['text'] ),
	'attr'     => [
		'name'  => 'param[screen]',
		'id'    => 'screen',
		'value' => isset( $param['screen'] ) ? $param['screen'] : '480',
		'min'   => '0',
		'step'  => '0.01',
	],
	'checkbox' => [
		'name'  => 'param[include_mobile]',
		'id'    => 'include_mobile',
		'value' => isset( $param['include_mobile'] ) ? $param['include_mobile'] : $mobil_show,
	],
	'addon'    => [
		'unit' => 'px',
	],
	'tooltip'     => esc_attr__( 'Specify the window breakpoint ( min width)', $this->plugin['text'] ),
);
//endregion

//region Users Role
$item_user = array(
	'label'   => esc_attr__( 'Show for users', $this->plugin['text'] ),
	'attr'    => [
		'type'  => 'select',
		'value' => esc_attr__( 'All Users', $this->plugin['text'] ),
	],
	'icon'    => '',
	'func'    => 'userRole',
);

//endregion

//region languages
$umodallang = isset( $param['umodallang '] ) ? $param['umodallang '] : '';

$lang = array(
	'label'    => esc_attr__( 'Enable language dependency', $this->plugin['text'] ),
	'attr'     => [
		'type'  => 'checkbox',
		'value' => '',
	],
	'tooltip'     => esc_attr__( 'Choose the language in which the notification will be displayed.', $this->plugin['text'] ),
	'icon'     => '',
	'func'     => '',
);
//endregion

//region Display

$show_option = array(
	'all'        => esc_attr__( 'All posts and pages', $this->plugin['text'] ),
	'shortecode' => esc_attr__( 'Where shortcode is inserted', $this->plugin['text'] ),
);

$herd_show = isset( $param['herd_show'] ) ? $param['herd_show'] : 'all';

$show = array(
	'label'   => esc_attr__( 'Display on', $this->plugin['text'] ),
	'attr'    => [
		'name'  => 'param[show]',
		'id'    => 'show',
		'value' => isset( $param['show'] ) ? $param['show'] : $herd_show,
	],
	'options' => $show_option,
	'tooltip'    => esc_attr__( 'Choose a condition to target to specific content.', $this->plugin['text'] ),
	'icon'    => '',
	'func'    => 'showChange',
);

//endregion

//region Other
$disable_fontawesome = array(
	'label' => esc_attr__( 'Disable Font Awesome 5', $this->plugin['text'] ),
	'attr'  => [
		'name'        => 'param[disable_fontawesome]',
		'id'          => 'disable_fontawesome',
		'value'       => isset( $param['disable_fontawesome'] ) ? $param['disable_fontawesome'] : '0',
	],
	'tooltip'  => esc_attr__( 'Disable Font Awesome 5 style on front-end of the site.', $this->plugin['text'] ),
	'icon'  => '',
);

$test_mode = array(
	'label'   => esc_attr__( 'Test mode', $this->plugin['text'] ),
	'attr'    => [
		'name'  => 'param[test_mode]',
		'id'    => 'test_mode',
		'value' => isset( $param['test_mode'] ) ? $param['test_mode'] : 'no',
	],
	'options' => [
		'no'  => esc_attr__( 'No', $this->plugin['text'] ),
		'yes' => esc_attr__( 'Yes', $this->plugin['text'] ),
	],
	'tooltip'    => esc_attr__( 'If test mode is enabled, the notification will show for admin only', $this->plugin['text'] ),
	'icon'    => '',
	'func'    => '',
);
//endregion