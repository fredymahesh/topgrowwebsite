=== Plugin Name ===
Contributors: harshitpeer
Donate link: harshitpeer.com
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

AIO Contact is a simple yet super usefull plugin which allows you to add multiple contact option for your end users

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `aio-contact.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place `<?php do_action('plugin_name_hook'); ?>` in your templates
