<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="dns-prefetch" href="//s.w.org">

<link rel="preload" href="<?php echo esc_url(get_theme_file_uri('assets/fonts/custom/Organey-headingBlack.woff2')); ?>" as="font" crossorigin>
<link rel="preload" href="<?php echo esc_url(get_theme_file_uri('assets/fonts/custom/Organey-headingBold.woff2')); ?>" as="font" crossorigin>
<link rel="preload" href="<?php echo esc_url(get_theme_file_uri('assets/fonts/custom/Organey-headingLight.woff2')); ?>" as="font" crossorigin>
<link rel="preload" href="<?php echo esc_url(get_theme_file_uri('assets/fonts/custom/Organey-headingMedium.woff2')); ?>" as="font" crossorigin>
<link rel="preload" href="<?php echo esc_url(get_theme_file_uri('assets/fonts/custom/Organey-headingRegular.woff2')); ?>" as="font" crossorigin>
<link rel="preload" href="<?php echo esc_url(get_theme_file_uri('assets/fonts/custom/Organey-headingSemiBold.woff2')); ?>" as="font" crossorigin>
<link rel="preload" href="<?php echo esc_url(get_theme_file_uri('assets/fonts/organey-icon.woff2')); ?>" as="font" crossorigin>
